# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_JuegoDados_session',
  :secret      => '66213b21a2a25c2c647135a5904c587552203d8daf95e9be6e3465e1ead42650f7e389db327ffd835790eefaac910236ac3a7c4af6f996a803a7a184da5f285f'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
# ActionController::Base.session_store = :active_record_store
